<?php

namespace Oliverbode\Cms\Plugin;

use Magento\Backend\Block\Widget\Button\ButtonList;
use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;

class Toolbar
{
    public function beforePushButtons(
        ToolbarContext $toolbar,
        AbstractBlock $context,
        ButtonList $buttonList
    ) {
        if ($context instanceof \Magento\Widget\Block\Adminhtml\Widget\Instance) {
            $buttonList->add('export', [
                'label' => __('Export Widgets'),
                'onclick' => 'setLocation(\'' .$context->getUrl('cms/export/widgets'). '\')',
                'class' => 'import-export'
            ]);
            $buttonList->add('import', [
                'label' => __('Import Widgets'),
                'onclick' => 'setLocation(\'' .$context->getUrl('cms/import/widgets'). '\')',
                'class' => 'import-export'
            ]);
        }
        return [$context, $buttonList];
    }
}