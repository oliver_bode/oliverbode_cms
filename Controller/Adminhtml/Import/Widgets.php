<?php
namespace Oliverbode\Cms\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;

class Widgets extends \Magento\Backend\App\Action
{

    protected $_coreRegistry = null;
    protected $resultPageFactory;
    protected $widgetFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     * @parma  \Magento\Widget\Model\Widget\Instance $widgetFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Widget\Model\Widget\Instance $widgetFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
        $this->_widgetFactory = $widgetFactory;
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    private function _toArray($xml) {
        $array = json_decode(json_encode($xml), true);
        foreach (array_slice($array, 0) as $key => $value) {
            if (strpos($key,'condition_') !== false) {
                unset($array[$key]);
                $key = str_replace('condition_','',$key);
            }
            if (strpos($key,'block_') !== false) {
                unset($array[$key]);
                $key = str_replace('block_','',$key);
            }
            if (is_string($value)) $array[$key] = trim($value);
            elseif (is_array($value) && empty($value)) $array[$key] = "";
            elseif (is_array($value)) {
                $array[$key] = $this->_toArray($value);
            }
        }
        return $array;
    }


    public function execute()
    {

        $default = BP . '/var/import/widgets.xml';
        $manual = BP . '/app/code/Oliverbode/Cms/data/widgets.xml';
        $composer = BP . '/vendor/oliverbode/cms/data/widgets.xml';
        if (file_exists($default)) $widgetFile = $default;
        else if (file_exists($manual)) $widgetFile = $manual;
        else if (file_exists($composer)) $widgetFile = $composer;
        else {
            $this->messageManager->addError(__('Import file doesn\'t exist: ' . $default)); 
            $this->_redirect('adminhtml/widget_instance/index');
            return;
        }
        $xml = simplexml_load_file($widgetFile);
        $widgetInstance = $this->_widgetFactory;
        if (!$widgetInstance) {
            $this->_redirect('adminhtml/widget_instance/index');
            return;
        }
        try {
            $count = 0;
            foreach ($xml->instance as $widget) {
                $widgetId = $widgetInstance->getCollection()
                    ->addFieldToFilter('title', $widget->title)
                    ->addFieldToFilter('instance_type', $widget->instance_type)
                    ->addFieldToFilter('theme_id', $widget->theme_id)
                    ->getFirstItem()
                    ->getId();
                $dataArray = $this->_toArray($widget);
                $data = array();
                foreach($dataArray as $key => $value) {
                    if ($key == 'instance_id') $data[$key] = $widgetId;
                    else if ($key == 'widget_parameters') {
                        $data[$key] = serialize($value);
                    }
                    else if ($key == 'page_groups') {
                        $i = 0;
                        foreach($dataArray['page_groups'] as $group) {
                            $data['page_groups'][$i] = $group;
                            $i++;
                        }
                    }
                    else if ($key == 'store_ids') {
                        for ($i = 0; $i < count($dataArray['store_ids']['store']); $i++) {
                            $data[$key][$i] = $dataArray['store_ids']['store'][$i];
                        }
                    }                    
                    else {
                        $data[$key] = $value;
                    }
                }
                $widgetInstance->setInstanceId(
                    $data['instance_id']
                )->setThemeId(
                    $data['theme_id']
                )->setTitle(
                    $data['title']
                )->setInstanceType(
                    $data['instance_type']
                )->setStoreIds(
                    $data['store_ids']
                )->setSortOrder(
                    $data['sort_order']
                )->setPageGroups(
                    $data['page_groups']
                )->setWidgetParameters(
                    $data['widget_parameters']
                );
                $widgetInstance->save();
                $count++;
            }
            $this->messageManager->addSuccess(__($count .' Widgets successfully imported.'));
            $this->_redirect('adminhtml/widget_instance/index');
        } catch (\Exception $exception) {
            $this->messageManager->addError($exception->getMessage());
            $this->_redirect('adminhtml/widget_instance/index');
            return;
        }
        $this->_redirect('adminhtml/widget_instance/index');
        return;
    }
}
