<?php
namespace Oliverbode\Cms\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;
use Magento\Cms\Model\Block;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class Blocks extends \Magento\Backend\App\Action
{

    protected $_coreRegistry = null;

    protected $resultPageFactory;
    
    protected $blockFactory;
    

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        Block $blockFactory 
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
        $this->_blockFactory = $blockFactory;
    }


    private function _toArray($xml) {
        $array = json_decode(json_encode($xml), true);
        foreach (array_slice($array, 0) as $key => $value) {
            if (is_string($value)) $array[$key] = trim($value);
            elseif (is_array($value) && empty($value)) $array[$key] = NULL;
            elseif (is_array($value)) {
                $array[$key] = $this->_toArray($value);
            }
        }
        return $array;
    }


    public function execute()
    {
        $default = BP . '/var/import/blocks.xml';
        $manual = BP . '/app/code/Oliverbode/Cms/data/blocks.xml';
        $composer = BP . '/vendor/oliverbode/cms/data/blocks.xml';
        if (file_exists($default)) $blocksFile = $default;
        else if (file_exists($manual)) $blocksFile = $manual;
        else if (file_exists($composer)) $blocksFile = $composer;
        else {
            $this->messageManager->addError(__('Import file doesn\'t exist: ' . $default)); 
            return $resultRedirect->setPath('cms/block/index');
        }
        $xml = simplexml_load_string(file_get_contents($blocksFile), null, LIBXML_NOCDATA);
        $cmsBlock = $this->_blockFactory;
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$cmsBlock) {
            return $resultRedirect->setPath('cms/block/index');
        }
        try {
            foreach ($xml->blocks as $block) {
                $blockId = $cmsBlock->getCollection()
                    ->addFieldToFilter('identifier', $block->identifier)
                    ->getFirstItem()
                    ->getId();
                $dataArray = $this->_toArray($block);
                $data = array();
                foreach($dataArray as $key => $value) {
                    if ($key == 'store_id') {
                        for ($i = 0; $i < count($dataArray['store_id']['store']); $i++) {
                            $data[$key][$i] = $dataArray['store_id']['store'][$i];
                        }
                    }
                    else $data[$key] = $value;
                }
                $cmsBlock->setBlockId($blockId);
                $cmsBlock->setData($data);
                $cmsBlock->save();
            }
            $this->messageManager->addSuccess(__('Cms Blocks successfully saved.'));
            return $resultRedirect->setPath('cms/block/index');
        } catch (\Exception $exception) {
            $this->messageManager->addError($exception->getMessage());
            return $resultRedirect->setPath('cms/block/index');
        }
        return $resultRedirect->setPath('cms/block/index');
    }
}
