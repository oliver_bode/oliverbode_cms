<?php
namespace Oliverbode\Cms\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;
use Magento\Cms\Model\Page;
use Magento\Cms\Controller\Adminhtml\Page\PostDataProcessor;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class Pages extends \Magento\Backend\App\Action
{

    protected $_coreRegistry = null;

    protected $resultPageFactory;
    
    protected $pageFactory;
    
    protected $dataProcessor;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        PostDataProcessor $dataProcessor,
        Page $pageFactory 
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->dataProcessor = $dataProcessor;
    }


    private function _toArray($xml) {
        $array = json_decode(json_encode($xml), true);
        foreach (array_slice($array, 0) as $key => $value) {
            if (is_string($value)) $array[$key] = trim($value);
            elseif (is_array($value) && empty($value)) $array[$key] = NULL;
            elseif (is_array($value)) {
                $array[$key] = $this->_toArray($value);
            }
        }
        return $array;
    }


    public function execute()
    {
        $default = BP . '/var/import/pages.xml';
        $manual = BP . '/app/code/Oliverbode/Cms/data/pages.xml';
        $composer = BP . '/vendor/oliverbode/cms/data/pages.xml';
        if (file_exists($default)) $pagesFile = $default;
        else if (file_exists($manual)) $pagesFile = $manual;
        else if (file_exists($composer)) $pagesFile = $composer;
        else {
            $this->messageManager->addError(__('Import file doesn\'t exist: ' . $default)); 
            return $resultRedirect->setPath('cms/page/index');
        }
        $xml = simplexml_load_string(file_get_contents($pagesFile), null, LIBXML_NOCDATA);
        $cmsPage = $this->_pageFactory;
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$cmsPage) {
            return $resultRedirect->setPath('cms/page/index');
        }
        try {
            foreach ($xml->pages as $page) {
                $pageId = $cmsPage->getCollection()
                    ->addFieldToFilter('identifier', $page->identifier)
                    ->getFirstItem()
                    ->getId();
                $dataArray = $this->_toArray($page);
                foreach($dataArray as $key => $value) {
                    if ($key == 'store_id') {
                        for ($i = 0; $i < count($dataArray['store_id']['store']); $i++) {
                            $data[$key][$i] = $dataArray['store_id']['store'][$i];
                        }
                    }
                    else $data[$key] = $value;
                }
                $cmsPage->setPageId($pageId);
                $cmsPage->setData($data);

                if (!$this->dataProcessor->validate($data)) {
                    $this->_redirect('adminhtml/cms/page/index');
                    return;
                }
                $cmsPage->save();
            }
            $this->messageManager->addSuccess(__('Cms Pages successfully saved.'));
            return $resultRedirect->setPath('cms/page/index');
        } catch (\Exception $exception) {
            $this->messageManager->addError($exception->getMessage());
            return $resultRedirect->setPath('cms/page/index');
        }
        return $resultRedirect->setPath('cms/page/index');
    }
}
