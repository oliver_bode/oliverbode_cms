<?php
namespace Oliverbode\Cms\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Widget\Model\ResourceModel\Widget\Instance\Collection;
use \Magento\Widget\Model\Widget\InstanceFactory;

class Widgets extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    protected $widgetCollection;

    protected $widgetFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Collection $widgetCollection,
        InstanceFactory $widgetFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_widgetCollection = $widgetCollection;
        $this->_widgetFactory = $widgetFactory;
    }

    public function execute()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>' . "\n";
        $xml .= '<root>' . "\n";
        $widgetCollection = $this->_widgetCollection;
        foreach($widgetCollection as $widget) {
            $xml .= "\t" . '<instance>' . "\n";
            $xml .= "\t\t" . '<instance_id>' . $widget->getInstanceId() . '</instance_id>' . "\n";
            $xml .= "\t\t" . '<instance_type>' . $widget->getInstanceType() . '</instance_type>' . "\n";
            $xml .= "\t\t" . '<theme_id>' . $widget->getThemeId() . '</theme_id>' . "\n";
            $xml .= "\t\t" . '<title>' . $widget->getTitle() . '</title>' . "\n";
            $xml .= "\t\t" . '<store_ids>' . "\n";
            $storeIds = $widget->getStoreIds();
            for ($i = 0; $i < count($storeIds); $i++) $xml .= "\t\t\t\t" . '<store>' . $storeIds[$i] . '</store>' . "\n";
            $xml .= "\t\t" . '</store_ids>' . "\n";
            $xml .= "\t\t" . '<widget_parameters>' ."\n";
            foreach ($widget->getWidgetParameters() as $key => $param) {
                if (is_array($param)) {
                $xml .= "\t\t\t" . '<' . $key . '>' . "\n"; 
                    foreach ($param as $ke => $val) {
                        if ($key == 'blocks') {
                            $xml .= "\t\t\t\t" . '<block_' . $ke . '>' . $val . '</block_' . $ke . '>' . "\n"; 
                        }
                        else if ($key == 'conditions') {
                            $xml .= "\t\t\t\t" . '<condition_' . $ke . '>' . "\n"; 
                            foreach ($val as $k => $va) {
                                $xml .= "\t\t\t\t\t" . '<' . $k . '>' . $va . '</' . $k . '>' . "\n";                                 
                            }
                            $xml .= "\t\t\t\t" . '</condition_' . $ke . '>' . "\n"; 
                        }
                    }
                $xml .= "\t\t\t" . '</' . $key . '>' . "\n";
                }
                else $xml .= "\t\t\t" . '<' . $key . '>' . $param . '</' . $key . '>' . "\n"; 
            }
            $xml .= "\t\t" . '</widget_parameters>' ."\n";
            $xml .= "\t\t" . '<sort_order>' . $widget->getSortOrder() . '</sort_order>' . "\n";
            $widgetPageGroups = $this->_widgetFactory
                ->create()
                ->load($widget->getInstanceId())
                ->setCode($widget->getCode())->getPageGroups();
            $xml .= "\t\t" . '<page_groups>' ."\n";
            for ($i = 0; $i < count($widgetPageGroups); $i++) {
                $xml .= "\t\t\t" . '<groups>' ."\n";
                $xml .= "\t\t\t\t" . '<page_group>' . $widgetPageGroups[$i]['page_group'] . '</page_group>'  . "\n";
                $xml .= "\t\t\t\t" . '<' . $widgetPageGroups[$i]['page_group'] . '>' . "\n";
                foreach ($widgetPageGroups[$i] as $key => $value) {
                    $key = str_replace(array('page_for','page_template','block_reference'),array('for','template','block'),$key);
                    if ($key != 'page_group') $xml .="\t\t\t\t\t" . '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
                }
                $xml .="\t\t\t\t" . '</' . $widgetPageGroups[$i]['page_group'] . '>' . "\n";
                $xml .="\t\t\t\t" . '<layout_handles>' . "\n";
                $xml .="\t\t\t\t\t" . '<layout_handle></layout_handle>' . "\n";
                $xml .="\t\t\t\t" . '</layout_handles>' . "\n";
                $xml .="\t\t\t" . '</groups>' . "\n";
            }
            $xml .= "\t\t" . '</page_groups>' ."\n";
            $xml .= "\t" . '</instance>' . "\n";
        }
        $xml .= '</root>' . "\n";
        header('Content-Type: text/xml');
        echo $xml;
    }
}
