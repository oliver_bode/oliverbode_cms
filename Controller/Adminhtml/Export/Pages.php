<?php
namespace Oliverbode\Cms\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Cms\Model\ResourceModel\Page\Collection;

class Pages extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    protected $pageCollection;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Collection $pageCollection
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_pageCollection = $pageCollection;
    }
    public function isCdata($key,$value) {
        if (!$value) return false;
        if ($key == 'content' ||  $key == 'layout_update_xml' || $key == 'custom_layout_update_xml' || $key == 'meta_keywords' || $key == 'meta_description') {
            return true;
        }
        return false;
    }

    public function execute()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>' . "\n";
        $xml .= '<root>' . "\n";
        $pageCollection = $this->_pageCollection->toArray();
        foreach($pageCollection['items'] as $page) {
            $xml .= "\t" . '<pages>' . "\n";
            foreach ($page as $key => $value)
            if (!is_array($value)) {
                $value = trim((string) $value);
                if ($this->isCdata($key,$value)) $xml .= "\t\t" . '<' . $key . '><![CDATA[' . $value . ']]></' . $key . '>' . "\n";
                else $xml .= "\t\t" . '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
            }
            else if ($key == 'store_id') {
                $xml .= "\t\t" . '<' . $key . '>' . "\n";
                foreach($value as $store) $xml .= "\t\t\t\t" . '<store>' . $store . '</store>' . "\n";
                $xml .= "\t\t" . '</' . $key . '>' . "\n";
            }  
            $xml .= "\t" . '</pages>' . "\n";
        }
        $xml .= '</root>' . "\n";
        header('Content-Type: text/xml');
        echo $xml;
    }
}
