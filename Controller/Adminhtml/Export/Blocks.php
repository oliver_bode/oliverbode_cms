<?php
namespace Oliverbode\Cms\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Cms\Model\ResourceModel\Block\Collection;

class Blocks extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    protected $blockCollection;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Collection $blockCollection
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_blockCollection = $blockCollection;
    }
    
    public function isCdata($key,$value) {
        if (!$value) return false;
        if ($key == 'content') {
            return true;
        }
        return false;
    }

    public function execute()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>' . "\n";
        $xml .= '<root>' . "\n";
        $blockCollection = $this->_blockCollection->toArray();

        foreach($blockCollection['items'] as $block) {
            $xml .= "\t" . '<blocks>' . "\n";
            foreach ($block as $key => $value)
            if (!is_array($value)) {
                $value = trim((string) $value);
                if ($this->isCdata($key,$value)) $xml .= "\t\t" . '<' . $key . '><![CDATA[' . $value . ']]></' . $key . '>' . "\n";
                else $xml .= "\t\t" . '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
            }
            else if ($key == 'store_id') {
                $xml .= "\t\t" . '<' . $key . '>' . "\n";
                foreach($value as $store) $xml .= "\t\t\t\t" . '<store>' . $store . '</store>' . "\n";
                $xml .= "\t\t" . '</' . $key . '>' . "\n";
            }  
            $xml .= "\t" . '</blocks>' . "\n";
        }

        $xml .= '</root>' . "\n";
        header('Content-Type: text/xml');
        echo $xml;
    }
}
