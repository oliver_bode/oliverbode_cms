# Magento2 CMS Importer/Exporter #

Magento2 CMS Importer/Exporter helps developers create and manage themes by converting cms data stored in database to xml files. These files can then be edited, placed in version control and imported back into the database.

## Features ##
* Well organized, easy to use and professionally designed.
* Adds Import and Export buttons to CMS Widget, Page and Block product grids

## Install CMS Importer/Exporter ##
### Manual Installation ###

Install CMS Importer/Exporter

* Download the extension
* Unzip the file
* Create a folder {Magento root}/app/code/Oliverbode/Cms
* Copy the content from the unzip folder

### Composer Installation ###

```
#!

composer config repositories.oliverbode_cms vcs https://oliver_bode@bitbucket.org/oliver_bode/oliverbode_cms.git
composer require oliverbode/cms
```

## Enable CMS Importer/Exporter ##

```
#!

php -f bin/magento module:enable --clear-static-content Oliverbode_Cms
php -f bin/magento setup:upgrade
```

## Using CMS Importer/Exporter ##

### Exporting CMS Data ###

Log into your Magetno Admin, then goto Content -> [Pages, Blocks, Widgets]

Use the Export button to export data to XML

### Importing CMS Data ###

Log into your Magetno Admin, then goto Content -> [Pages, Blocks, Widgets]

Use the Import button to import data saved in:

### Manual Install ###
* /Magento-Install-Dir/Oliverbode/Cms/data/pages.xml
* /Magento-Install-Dir/Oliverbode/Cms/data/blocks.xml
* /Magento-Install-Dir/Oliverbode/Cms/data/widgets.xml

### Composer Install ###
* /Magento-Install-Dir/vendor/oliverbode/cms/data/pages.xml
* /Magento-Install-Dir/vendor/oliverbode/cms/data/blocks.xml
* /Magento-Install-Dir/vendor/oliverbode/cms/data/widgets.xml